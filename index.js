// 1. What directive is used by Node.js in loading the modules it needs?
// Node.js uses the require directive to load the modules it needs


// 2. What Node.js module contains a method for server creation?
// The http module in Node.js contains a method for server creation


// 3. What is the method of the http object responsible for creating a server using Node.js?
// The method of the http object in Node.js responsible for creating a server is createServer()


// 4. What method of the response object allows us to set status codes and content types?
	// The writeHead method of the response object in Node.js allows you to set the status code and content type for an HTTP response.

// 5. Where will console.log() output its contents when run in Node.js?
// The console.log() output in Node.js is displayed in the terminal or command prompt where the Node.js process is running


// 6. What property of the request object contains the address's endpoint?
// In a Node.js/Express application, the property of the request object that contains the endpoint address is req.originalUrl

// 7. Create a server using the createServer method that will listen in to the port provided above.
const http = require('http');
const port = 3000;

const server = http.createServer((request, response) => {
    if (request.url === '/login') { // 9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Welcome to the login page\n');
    } else { // 11. Create a condition for any other routes that will return an error message.
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end("I'm sorry the page you are looking for cannot be found\n");
      }
  });

server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

// 8. Console log in the terminal a message when the server is successfully running.
server.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
  });

// 10. Access the login route to test if it’s working as intended.
curl http://localhost:3000/login
  
// 12. Access any other route to test if it’s working as intended.

curl http://localhost:3000/